\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{gensymb}
\usepackage[margin=1.87cm]{geometry}
\usepackage{indentfirst}
\usepackage{cite}
\usepackage{hyperref}
\usepackage{float}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}
\usepackage{cleveref}
\usepackage{fancyhdr}
\usepackage{filemod}
\usepackage{url}
\pagestyle{fancy}
\lhead{Sandeep vs. Metamom}
\chead{}
\rhead{Stewart Pearson}
\lfoot{Last Modified: \filemodprintdate{\jobname}}
\cfoot{}
\rfoot{\thepage}
\usepackage{svg}
\title{Comparing Traditional Method of Moments Formulation with the one used by Sandeep}
\date{February 16, 2020}
\author{Stewart Pearson \\ Supervisor: Prof. Sean V. Hum}

\DeclareMathOperator*{\maximize}{{maximize}}
\DeclareMathOperator*{\minimize}{{minimize}}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\begin{document}
\maketitle

%----------------------------------------------------------------------------------------
\section{Overview} 
The purpose of this exercise is to try to reconcile the method of moments formulation presented by Sandeep \cite{Sandeep2018} with the one we were using before. The main difference is that in the integral equations, Sandeep uses fields instead of currents, while also using susceptibilities. The goal was to use Selvanayagam's analytical impedances \cite{Selvanayagam2013} to do plane wave refraction on a planar surface with Sandeep's MoM equations. 

%----------------------------------------------------------------------------------------
\section{Deriving field relation (A matrix) from impedances}
The A matrix from Sandeep relates the fields on one side of the cylinder to the other through the susceptibilities. To derive it, we must start from the GSTCs for the fields in \Cref{SelvanayagamFields}. 
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=\textwidth]{images/SelvanayagamFields}
	\caption{Incident, scattered, and transmitted fields from Fig. 10 of Selvanayagam \cite{Selvanayagam2013}}
	\label{SelvanayagamFields}
\end{figure}
We first start with the GSTC presented by Epstein \cite{Epstein2016} and form a matrix equation for it.
\begin{align}
\textbf{E}_{t,avg} &= \textbf{Z}_{se}(\hat{x}\times \Delta \textbf{H}_t)\\
\begin{bmatrix}
\hat{x}\times \Delta \textbf{H}_y \\
\hat{x}\times \Delta \textbf{H}_z
\end{bmatrix} &= 
\begin{bmatrix}
\textbf{Z}_{se}^{-1} & 0\\
0 & \textbf{Z}_{se}^{-1} 
\end{bmatrix} 
\begin{bmatrix}
\textbf{E}_{y,avg}\\
\textbf{E}_{z,avg}
\end{bmatrix} \\
\begin{bmatrix}
\Delta \textbf{H}_z \\
-\Delta \textbf{H}_y
\end{bmatrix} &= 
\begin{bmatrix}
\frac{1}{2\textbf{Z}_{se}} & 0\\
0 & \frac{1}{2\textbf{Z}_{se}}
\end{bmatrix} 
\begin{bmatrix}
\textbf{E}_{y,1}+\textbf{E}_{y,2}\\
\textbf{E}_{z,1}+\textbf{E}_{z,2}
\end{bmatrix} 
\end{align}
Similarly, we can write a matrix equation for the magnetic side as well.
\begin{align}
\textbf{H}_{t,avg} &= -\textbf{Y}_{sm}(\hat{x}\times \Delta \textbf{E}_t)\\
\begin{bmatrix}
\hat{x}\times \Delta \textbf{E}_y \\
\hat{x}\times \Delta \textbf{E}_z
\end{bmatrix} &= 
\begin{bmatrix}
-\textbf{Y}_{sm}^{-1} & 0\\
0 & -\textbf{Y}_{sm}^{-1} 
\end{bmatrix} 
\begin{bmatrix}
\textbf{H}_{y,avg}\\
\textbf{H}_{z,avg}
\end{bmatrix} \\
\begin{bmatrix}
\Delta \textbf{E}_z \\
-\Delta \textbf{E}_y
\end{bmatrix} &= 
\begin{bmatrix}
-\frac{1}{2\textbf{Y}_{sm}} & 0\\
0 & -\frac{1}{2\textbf{Y}_{sm}}
\end{bmatrix} 
\begin{bmatrix}
\textbf{H}_{y,1}+\textbf{H}_{y,2}\\
\textbf{H}_{z,1}+\textbf{H}_{z,2}
\end{bmatrix} 
\end{align}
Using these two matrix equations, we can now write our version of the A matrices from Sandeep. 
\begin{align}
\begin{bmatrix}
0 & 0 & 0 & 1\\
-\frac{1}{2\textbf{Z}_{se}} & -1 & 0 & 0\\
-1 & -\frac{1}{2\textbf{Y}_{sm}} & 0 & 0\\
0 & 0 & 1 & 0\\
\end{bmatrix} 
\begin{bmatrix}
\textbf{E}_{z,2}\\
\textbf{H}_{y,2}\\
\textbf{E}_{y,2}\\
\textbf{H}_{z,2}
\end{bmatrix} &=  
\begin{bmatrix}
0 & 0 & 0 & 1\\
\frac{1}{2\textbf{Z}_{se}} & -1 & 0 & 0\\
-1 & \frac{1}{2\textbf{Y}_{sm}} & 0 & 0\\
0 & 0 & 1 & 0\\
\end{bmatrix} 
\begin{bmatrix}
\textbf{E}_{z,1}\\
\textbf{H}_{y,1}\\
\textbf{E}_{y,1}\\
\textbf{H}_{z,1}
\end{bmatrix}\\
\textbf{A} &= \textbf{A}_2^{-1}\textbf{A}_1\\
\begin{bmatrix}
\textbf{E}_{z,2}\\
\textbf{H}_{y,2}\\
\textbf{E}_{y,2}\\
\textbf{H}_{z,2}
\end{bmatrix} &=  
\begin{bmatrix}
\frac{4\textbf{Y}_{sm}\textbf{Z}_{se}+1}{4\textbf{Y}_{sm}\textbf{Z}_{se}-1} & \frac{-4\textbf{Z}_{se}}{4\textbf{Y}_{sm}\textbf{Z}_{se}-1} & 0 & 0\\
\frac{-4\textbf{Y}_{sm}}{4\textbf{Y}_{sm}\textbf{Z}_{se}-1} & \frac{4\textbf{Y}_{sm}\textbf{Z}_{se}+1}{4\textbf{Y}_{sm}\textbf{Z}_{se}-1} & 0 & 0\\
0 & 0 & 1 & 0\\
0 & 0 & 0 & 1\\
\end{bmatrix} 
\begin{bmatrix}
\textbf{E}_{z,1}\\
\textbf{H}_{y,1}\\
\textbf{E}_{y,1}\\
\textbf{H}_{z,1}
\end{bmatrix}
\end{align}
for the TE case $\textbf{E}_y = \textbf{E}_x = \textbf{H}_z = 0$ so it can be simplified to just the top right $2\times 2$ matrix.
%----------------------------------------------------------------------------------------
\section{Deriving \textbf{Z} matrix for a sheet instead of a cylinder}
We can start with the integral equation arising from Green's second scalar theorem (eqn. 10.2.21 in Ch. 10 of Jin' book\cite{Jin2010}) in the TE case,
\begin{align}
\textbf{E}_z^{inc} + \oint_{\Gamma _0} [\textbf{E}_{z} \frac{\partial G_0(\rho,\rho')}{\partial x'} - G_0(\rho,\rho')\frac{\partial \textbf{E}_z}{\partial x'}] d\Gamma _0 = \frac{1}{2}\textbf{E}_z
\end{align}
we then split it into a top and bottom integral for both sides of the surface
\begin{align}
\textbf{E}_z^{inc} + \int_{\Gamma ^+} [\textbf{E}_{z,1} \frac{\partial G_0(\rho,\rho')}{\partial x'} - G_0(\rho,\rho')\frac{\partial \textbf{E}_{z,1}}{\partial x'}] d\Gamma ^+  + \int_{\Gamma ^-} [\textbf{E}_{z,2} \frac{\partial G_0(\rho,\rho')}{\partial x'} - G_0(\rho,\rho')\frac{\partial \textbf{E}_{z,2}}{\partial x'}] d\Gamma ^- = \frac{1}{2}\textbf{E}_z
\end{align}
which we then split into separate top and bottom integrals
\begin{subequations}
\begin{align}
\textbf{E}_{z,1}^{inc} + \int_{\Gamma ^+} [\textbf{E}_{z,1} \frac{\partial G_0(\rho,\rho')}{\partial x'} - G_0(\rho,\rho')\frac{\partial \textbf{E}_{z,1}}{\partial x'}] d\Gamma ^+ &=  \frac{1}{2}\textbf{E}_{z,1}\\
\textbf{E}_{z,2}^{inc} + \int_{\Gamma ^-} [\textbf{E}_{z,2} \frac{\partial G_0(\rho,\rho')}{\partial x'} - G_0(\rho,\rho')\frac{\partial \textbf{E}_{z,2}}{\partial x'}] d\Gamma ^- &= \frac{1}{2}\textbf{E}_{z,2}
\end{align}
\end{subequations}
For plane wave refraction, $\textbf{E}_{z,2}^{inc} = 0$. The derivatives can now be evaluated for both the top and bottom. 
\begin{align}
\frac{\partial}{\partial x'} \frac{H_0^{(2)}(k\sqrt{(x-x')^2+(y-y')^2})}{4j} &= \frac{-H_1^{(2)}(k\sqrt{(x-x')^2+(y-y')^2})}{4j} \frac{-k(x-x')}{\sqrt{(x-x')^2+(y-y')^2}}\\
\frac{\partial}{\partial x'} \frac{H_0^{(2)}(k\sqrt{(x-x')^2+(y-y')^2})}{4j} &= \frac{-jH_1^{(2)}(k\sqrt{(x-x')^2+(y-y')^2})k(x-x')}{4\sqrt{(x-x')^2+(y-y')^2}} 
\end{align}
Now subbing this in and using $x'=0$, $x^-=-dx$, and $x^+=dx$ where $dx$ is half the thickness of the sheet,
\begin{subequations}
\begin{align}
{E}_{z,1}^{inc} &= \frac{1}{2}{E}_{z,1} - \int_{-\frac{l}{2}}^{\frac{l}{2}} [E_{z,1} \frac{jk H_1^{(2)}(k\sqrt{(x-x')^2+(y-y')^2})dx}{4\sqrt{(x-x')^2+(y-y')^2}} - \frac{H_0^{(1)}(k\sqrt{(x-x')^2+(y-y')^2})}{4j}j\omega\mu H_{y,1}]dy' \\
0 &= \frac{1}{2}{E}_{z,2} - \int_{-\frac{l}{2}}^{\frac{l}{2}} [E_{z,2} \frac{-jk H_1^{(2)}(k\sqrt{(x-x')^2+(y-y')^2})dx}{4\sqrt{(x-x')^2+(y-y')^2}} - \frac{H_0^{(1)}(k\sqrt{(x-x')^2+(y-y')^2})}{4j}j\omega\mu (-H_{y,2})]dy' 
\end{align}
\end{subequations}
where the sign on $H_{y,2}$ is negative from the direction in \Cref{SelvanayagamFields}. We can write this using Sandeep's notation as 
\begin{subequations}
\begin{align}
p_{mn} E_{z,1} + r_{mn} H_{y,1} &= E_{z,1}^{inc} \\
[A_{1,1,n}q_{mn} -A_{2,1,n}r_{mn}]E_{z,1} + [A_{2,1,n}q_{mn} - A_{2,2,n}r_{mn}]H_{y,1} &= 0
\end{align}
\end{subequations}
which can finally be written as an MoM matrix equation,
\begin{align}
\begin{bmatrix}
p_{mn} & r_{mn}\\
A_{1,1,n}q_{mn} -A_{2,1,n}r_{mn} & A_{2,1,n}q_{mn} - A_{2,2,n}r_{mn}
\end{bmatrix}
\begin{bmatrix}
E_{z,1} \\
H_{y,1}
\end{bmatrix} = 
\begin{bmatrix}
E_{z,1^{inc}} \\
0
\end{bmatrix}
\end{align}
to solve for the fields on side 1. After those fields are found, you can use the \textbf{A} matrix to solve for the fields on side 2.


%----------------------------------------------------------------------------------------
\section{Verifying with Matlab}
In order to make sure the derived \textbf{A} matrix and integral equations are correct the results were verified in Matlab and compared to metamom.m. The test case was TE plane wave refraction to $45^{\circ}$. The sheet thickness ($2\times dx$) used was $dx=dy/1000000$ where $dy$ is the discretization step on the surface ($dy=0.01\lambda$). \textbf{Note:} you can also make the surface thickness zero ($dx=0$), which give similar results (field amplitude of 2.508 at $45^{\circ}$).

\subsection{Results}
The results after simulations for far field and surface currents are shown in \Cref{MetamomFF,SandeepFF,CurrentCompare}.
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=\textwidth]{images/MetamomFF}
	\caption{Far field pattern using metamom.m MoM Formulation}
	\label{MetamomFF}
\end{figure}
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=\textwidth]{images/SandeepFF}
	\caption{Far field pattern using Sandeep MoM Formulation}
	\label{SandeepFF}
\end{figure}
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=\textwidth]{images/CurrentCompare}
	\caption{Surface current comparison between Sandeep and metamom.m}
	\label{CurrentCompare}
\end{figure}

The far field patterns agree very well, with the difference in magnitude adjustable by varying the surface thickness in the Sandeep case. The surface currents are also close, albeit not exactly the same. 

In conclusion, we can see that the Sandeep formulation agrees well with our own. Unfortunately, due to the complicated formulation, I don't think we can use it for optimization moving forward.


\newpage
\bibliographystyle{ieeetr}
\bibliography{../../../Mendeley/library}
\end{document}







 










