% This function takes in the far field radius, vector of angles, vector of 
% currents, and EM (1 for E, 0 for M). It returns
% the far field E or M at R.
% Author:   Stewart Pearson
% Date:     July 25, 2019

function FF = GetFF(R,phi, y, k, I, EorM)
    M = size(phi,1);
    N = size(I,1);
    mu0 = 4*pi*1e-7;
    eps0 = 8.854e-12;
    eta0 = 120*pi;
    w = k*3e8;
    
    for m = 1:M
        RR = ones(N, 1)*R;
        rho = [RR*cos(phi(m)) RR*sin(phi(m))];
        rhop = [zeros(N, 1), y];
        rhodiff = rho-rhop;
        ard = sqrt(sum(rhodiff.^2, 2));
        % OLD
        %%Ese(m) = -w*mu0/4*trapz(y, Ie.*besselh(0, 2, k*ard));
        %%Esm(m) = 1j/4*trapz(y, Im.*besselh(1, 2, k*ard).*k.*rho(:, 1)./ard);
        % NEW based on Gibson ch. 3
        if EorM == 1
            Aff(m) = -1j*mu0*sqrt(1j/(8*pi*k)) * trapz(y, I.*exp(1j*k*y*sin(phi(m))));
%             Aff(m) = -1j*mu0*sqrt(1/(8*pi*k)) * trapz(y, I.*exp(1j*k*y*sin(phi(m))));
            FF(m) = -1j*w*Aff(m);
        else
            Fff(m) = -1j*eps0*sqrt(1j/(8*pi*k)) * trapz(y, I.*exp(1j*k*y*sin(phi(m)))) * cos(phi(m));
%             Fff(m) = -1j*eps0*sqrt(1/(8*pi*k)) * trapz(y, I.*exp(1j*k*y*sin(phi(m)))) * cos(phi(m));
            FF(m) = 1j*w*eta0*Fff(m);
        end        
    end
    
    
    