% METAMOM 
%   

% Author: Sean Victor Hum
% Updated: March 2016
%
% Compared to metamom.m, I believe the magnetic terms are properly
% derived in here according to my note metasurface_ie.pdf.

clear all

L = 3;                                 % Length in wavelengths
dy = 0.01;
g = 1.781;
k = 2*pi;                               % Assuming wavelength = 1m 
w = k*3e8;                              % Assuming wavelength = 1m
mu0 = 4*pi*1e-7;
eps0 = 8.854e-12;
eta0 = 120*pi;
N = L/dy+1;                             % Number of points in sheet
Np = 721;                           % Number of points in far-field
Np = 361;                           % Number of points in far-field
dphi = 2*pi/(Np-1);
phi = (0:Np-1)'/(Np-1) * 2*pi;
R = 1000;                           % Far-field measurement radius
Ei = 1;                                 % Incident electric field strength

% Impedance matrix calculations for self and near terms (Gibson
% 5.15, 5.16)
self_e = dy*(1-1j*2/pi*log(g*k*dy/(4*exp(1))));
near_e = dy - 1j*dy/pi*(3*log(3*g*k*dy/4) - log(g*k*dy/4) - 2); 
%self_m = self_e*(k^2-0.5) + 0.5*1j*16/(pi*k^2*dx);
%near_m = near_e*(k^2-0.5) - 0.5*1j*16/(3*pi*k^2*dx);
self_m = k^2/2*(dy + k^2*dy^3/96 - 1j*dy/pi*(2*log(g*k*dy/4/exp(1)) ...
                                             + 16/(k*dy)^2));

y = (0:N-1)'*dy-L/2;
dy = abs(y(1) - y(2));
Ze = zeros(N);

Nf = 21;
dxx = dy/(Nf-1);

phi_i = 0;                              % Incident angle 
phi_t = 45/180*pi;                      % Desired refraction angle

% Work out analytical impedance and admittance functions required
Phi = k*y*(sin(phi_t) - sin(phi_i));
Xe = -120*pi/(2*cos(phi_t))*cot(Phi/2);
Xm = -cos(phi_t)/(2*eta0)*cot(Phi/2);

% Limit maximum values of Xe and Bm to avoid singular loaded
% impedance matrices
Xe(find(Xe > 1e5)) = 1e5;
Xe(find(Xe < -1e5)) = -1e5;
Xm(find(Xm > 1)) = 1;
Xm(find(Xm < -1)) = -1;

% Impedance matrix fill
for m = 1:N
    for n = 1:N
        yp = (0:Nf-1)*dxx + (n-1)*dy; 
        if (m == n)
            Ze(m, n) = self_e;
            Zm(m, n) = self_m;
            %intg = besselh(0, 2, k*abs(y(m) - yp));
            %keyboard
            %Z(m, n) = trapz(yp, intg);
        elseif (abs(m-n)==1)
            Ze(m, n) = near_e;
            %Zm(m, n) = near_m;
            Zm(m, n) = k^2/2*dy*(1 + 4j/(pi*k^2*(abs(y(m) - y(n))^2 - dy^2/4)));
         elseif (abs(m-n) == 2)
            Ze(m, n) = besselh(0, 2, k*abs(y(m) - y(n)))*dy;
            Zm(m, n) = k^2/2*dy*(1 + 4j/(pi*k^2*(abs(y(m) - y(n))^2 - dy^2/4)));
        else
            Ze(m, n) = besselh(0, 2, k*abs(y(m) - y(n)))*dy;
            %Zm(m, n) = Ze(m, n)*(k^2-0.5) + 0.5*besselh(2, 2, k*(y(m) - y(n)))*dx;
            Zm(m, n) = dy*k/abs(y(m)-y(n))*besselh(1, 2, k*abs(y(m)-y(n)));
            %Zm(m, n) = k^2*Ze(m, n);
        end
    end
end

% Calculate incident fields 
Einc = Ei*exp(-1j*k*y*sin(phi_i));
Hinc = -Ei/eta0*cos(phi_i)*exp(-1j*k*y*sin(phi_i));

% Calculate incident power (space domain)
Pinc = 0.5*Einc.*conj(Hinc);
Winc = -trapz(y, Pinc);

% Incident field calculation (for far field) using Schelkunoff PMC
% equivalence principle 
Jeq_inc = 2*Hinc;
for m = 1:Np
    RR = ones(N, 1)*R;
    rho = [RR*cos(phi(m)) RR*sin(phi(m))];
    rhop = [zeros(N, 1), y];
    rhodiff = rho-rhop;
    ard = sqrt(sum(rhodiff.^2, 2));
    % OLD
    %Einc_ff(m) = -w*mu0/4*trapz(y, Jeq_inc.*besselh(0, 2, k*ard));
    % NEW based on Gibson ch. 3 (3.140)
    Ainc_ff(m) = -1j*mu0*sqrt(1j/(8*pi*k)) * trapz(y, Jeq_inc.*exp(1j*k*y*sin(phi(m))));
    for n = 1:N
        G(m,n) = -mu0*w*sqrt(1j/(8*pi*k)) * exp(1j*k*y(n)*sin(phi(m))) * dy;
    end
    % Pinc_ff.*R.*sin(phi)
    Einc_ff(m) = -1j*w*Ainc_ff(m);
    Pinc_ff(m) = 0.5*abs(Einc_ff(m))^2/eta0;
end
% Zero out fields behind PMC
rng_out = find(phi >= pi/2 & phi <= 3*pi/2);
Einc_ff_full = Einc_ff;
Einc_ff(rng_out) = 0;
% Instead just use Einc_ff(rng)

% Specify incident field (voltage vector) and load impedance matrix
% as required
be_inc = 4/(mu0*w)*Einc;
bm_inc = 4*w*mu0*Hinc;
re = 4/(mu0*w)*1j*Xe;
rm = 4*w*mu0*1j*Xm;
Re = diag(re);
Rm = diag(rm);
Zel = Ze + Re;
Zml = Zm + Rm;
Ie = Zel\be_inc;
Im = Zml\bm_inc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Transform Impedances to Susceptibilities %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Xee = (1j*Xe).^(-1)./(1i*w*eps0);
Xmm = -(1j*Xm).^(-1)./(1i*w*mu0); %Double check negative

% Limit maximum values of Xe and Xm to avoid singular loaded
% impedance matrices
Xee(find(abs(Xee) > 1e5)) = 1e10;
Xee(find(abs(Xee) < -1e5)) = -1e10;
Xmm(find(abs(Xmm) > 1e5)) = 1e10;
Xmm(find(abs(Xmm) < -1e5)) = -1e10;

%%%%%%%%%%%%%%%%%%%
% Define A matrix %
%%%%%%%%%%%%%%%%%%%
q = w^2*eps0*mu0; 
for p = 1:length(Xee)   % Susceptance method
        d = Xee(p)*Xmm(p)*q+4;
        Ax(1,1,p) = (4-Xee(p)*Xmm(p)*q)/(d);
        Ax(1,2,p) = -(1i*4*w*mu0*Xmm(p))/(d);
        Ax(2,1,p) = -(1i*4*w*eps0*Xee(p))/(d);
        Ax(2,2,p) = (4-Xee(p)*Xmm(p)*q)/(d);
end
Zse = 1j*Xe;
Zsm = 1j*Xm;
for p = 1:length(re)    % Impedance method
        denom = 4*Zse(p)*Zsm(p)-1;
        A(1,1,p) = (4*Zse(p)*Zsm(p)+1)/denom;
        A(1,2,p) = -(4*Zse(p))/denom;
        A(2,1,p) = -(4*Zsm(p))/denom;
        A(2,2,p) = (4*Zse(p)*Zsm(p)+1)/denom;
end

%%%%%%%%%%%%%%%%%%%
% Define Z Matrix %
%%%%%%%%%%%%%%%%%%%

% This code is borrowed from Nicolas and is based off of the Sandeep paper
% eqn. 14
%Define location of each element along cylinder (currently not used)

dx = dy/1000000; % thickness of sheet
dx = 0;

tic;
%Define constants
% kt = a*k*sqrt(2);
% b = g*kt/2;
self_e = dy*(1-1j*2/pi*log(g*k*dy/(4*exp(1))));
near_e = dy - 1j*dy/pi*(3*log(3*g*k*dy/4) - log(g*k*dy/4) - 2); 
self_m = k^2/2*(dy + k^2*dy^3/96 - 1j*dy/pi*(2*log(g*k*dy/4/exp(1)) + 16/(k*dy)^2));
N1 = 1000; %Number of Elements for Integrals
for m = 1:N %Step through columns
    for n = 1:N %Step through rows
        rho_diff_mag = sqrt((dx)^2+(y(m)+dy/2-y(n))^2);
        
        if abs(m-n) == 0 %Self Terms           

           %Define intermidiate variables
           y_LB = y(n); %Lower Bound
           y_UB = y(n)+dy; %Upper Bound
           y_to_int = linspace(y_LB,y_UB,N1);
           integrand_rmn = besselh(0,2,k*sqrt(dx^2 + (y(m)+dy/2-y_to_int).^2));
           integrand_pmn = 1./sqrt(dx^2 + (y(m)+dy/2-y_to_int).^2).*besselh(1,2,k*sqrt(dx^2 + (y(m)+dy/2-y_to_int).^2));

           %Compute Coeffients
           pmn = 0.5 - (1j*k*dx/(4))*trapz(y_to_int,integrand_pmn);
           qmn = 0.5 + (1j*k*dx/(4))*trapz(y_to_int,integrand_pmn);
           rmn = w*mu0/4*trapz(y_to_int,integrand_rmn);

        elseif abs(m-n) < 2 %Adjacent terms

           %Define intermidiate variables
           y_LB = y(n); %Lower Bound
           y_UB = y(n)+dy; %Upper Bound
           y_to_int = linspace(y_LB,y_UB,N1);
           integrand_rmn = besselh(0,2,k*sqrt(dx^2 + (y(m)+dy/2-y_to_int).^2));
           integrand_pmn = 1./sqrt(dx^2 + (y(m)+dy/2-y_to_int).^2).*besselh(1,2,k*sqrt(dx^2 + (y(m)+dy/2-y_to_int).^2));

           %Compute Coeffients
           pmn = - (1j*k*dx/(4))*trapz(y_to_int,integrand_pmn);
           qmn = + (1j*k*dx/(4))*trapz(y_to_int,integrand_pmn);
           rmn = w*mu0/4*trapz(y_to_int,integrand_rmn);

        else %Off-diagional terms
           qmn =  (1j*k*dx)/(4*abs(rho_diff_mag))*besselh(1, 2, k*rho_diff_mag)*dy;
           pmn = -qmn;
           rmn = w*mu0/4*besselh(0, 2, k*rho_diff_mag)*dy;

        end
       %Populate Matrix
        Z(m,n) = pmn; %Z11
        Z(m,n+N) = rmn; %Z12
        Z(m+N,n) = A(1,1,n)*(qmn)-A(2,1,n)*(rmn); %Z21
        Z(m+N,n+N) = A(1,2,n)*(qmn)-A(2,2,n)*(rmn); %Z22
   end 
end

b = [Einc; zeros(size(Einc))]; % I think Sandeep has [Ez1_inc; Ez2_inc]
F1 = Z\b;
for idx = 1:size(A,3)
    F2([idx; idx+N],:) = A(:,:,idx)*F1([idx; idx+N]);
end

Ie_Sandeep = -(F2(N+1:end)-F1(N+1:end)); % Equivalent currents from the tangential fields
Im_Sandeep = (F2(1:N)-F1(1:N)); % Equivalent currents from the tangential fields Ms = -x_hat x DeltaEz (eq. 3 in Epstein 2016)

% Plot currents to compare methods
figure; 
subplot(2,1,1)
plot(y, abs(Ie))
hold on
plot(y, abs(Ie_Sandeep))
legend('|Ie| Metamom', '|Ie| Sandeep')
xlabel('y [\lambda]')
ylabel('Current [A]')
title('Electric currents comparison')
subplot(2,1,2)
plot(y, abs(Im))
hold on
plot(y, abs(Im_Sandeep))
legend('|Im| Metamom', '|Im| Sandeep')
xlabel('y [\lambda]')
ylabel('Current [A]')
title('Magnetic currents comparison')

% Look at far field patterns
Ese_Sandeep = GetFF(R, phi, y, k, Ie_Sandeep, 1);
Ese = GetFF(R, phi, y, k, Ie, 1);
Esm_Sandeep = GetFF(R, phi, y, k, Im_Sandeep, 0);
Esm = GetFF(R, phi, y, k, Im, 0);
Es_Sandeep = Ese_Sandeep+Esm_Sandeep;
Es = Ese+Esm;
figure; 
polarplot(phi, abs(Es_Sandeep+Einc_ff),'LineWidth', 2);
hold on
polarplot(phi, abs(Ese_Sandeep),'--');
polarplot(phi, abs(Esm_Sandeep),'--');
polarplot(phi, abs(Einc_ff),'--');
legend('Total Field', 'Ese', 'Esm', 'Einc_f_f')
title('Far field plot from equivalent surface currents [Sandeep]')
figure; 
polarplot(phi, abs(Es+Einc_ff),'LineWidth', 2);
hold on
polarplot(phi, abs(Ese),'--');
polarplot(phi, abs(Esm),'--');
polarplot(phi, abs(Einc_ff),'--');
legend('Total Field', 'Ese', 'Esm', 'Einc_f_f')
title('Far field plot from equivalent surface currents [Metamom]')





