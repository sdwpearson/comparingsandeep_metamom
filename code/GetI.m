% This function takes in the incident field, impedance matrix, and loading
% and then solves for the currents.
% Author:   Stewart Pearson
% Date:     July 25, 2019

function I = GetI(b, Z, X)
    Re = diag(X);
    Zel = Z + Re;
    I = Zel\b; %invert the Z matrix to find the surface currents
    return I;
end